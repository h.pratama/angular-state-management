import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import { UsersService } from '../services/users.service';
import { selectorUser } from '../store/users/users.reducer';
import { setUser, User } from '../store/users/users.actions';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  user:User = {
    id: '',
    name: '',
    avatar: '',
    createdAt: '',
    password: '',
    email: '',
  };

  loading:boolean = true;

  getUser:Observable<User>;

  constructor(
    private store: Store,
    private userService: UsersService,
    private route: ActivatedRoute,
  ) { 
    this.getUser = store.select(selectorUser)
  }

  ngOnInit(): void {
    this.store.dispatch(setUser(this.user))

    const getParam = this.route.snapshot.paramMap.get('id')!;

    this.userService.getUser(getParam)

    this.getUser.subscribe(data => {
      if (data.name) {
        this.loading = false
      }
    })

    // this.store.select(selectorUser).subscribe(data => {
    //   console.log(data, '<< data user');
    //   this.user = data
    // }
    // )

  }

}
