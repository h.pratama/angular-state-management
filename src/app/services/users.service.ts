import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { setUser, setUsers, User } from '../store/users/users.actions';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  urlApi = 'https://63478aa3db76843976ad4a8a.mockapi.io/api/vq/users'

  constructor(
    private http: HttpClient,
    private store: Store
  ) { }

  getUsers() {
    this.http.get(this.urlApi)
    .subscribe((data:any) => {
      if (data?.length > 0) {
        this.store.dispatch(setUsers({data}))
      }
    })
  }

  getUser(id: string) {
    this.http.get(`${this.urlApi}/${id}`)
    .subscribe((data:any) => {
      if ('name' in  data) {
        this.store.dispatch(setUser(data))
      }
    })
  }
}
