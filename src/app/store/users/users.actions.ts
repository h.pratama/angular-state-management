import { createAction, props } from "@ngrx/store";

export interface User {
    createdAt: string;
    name: string;
    email: string;
    password: string;
    avatar: string;
    id: string;
}

export const setUsers = createAction(
    '[Users Component] Set Users',
    props<{data: User[]}>()
)

export const setUser = createAction(
    '[Users Component] Set User',
    props<User>()
)
