import { createFeatureSelector, createReducer, createSelector, on } from "@ngrx/store";
import { setUser, setUsers } from "./users.actions";
import { User } from "./users.actions";

interface UserState {
    users: User[],
    user: User,
}


export const initialState:UserState = {
    users: [],
    user: {
        id: '',
        name: '',
        email: '',
        password: '',
        avatar: '',
        createdAt: ''
    }
}

export const userReducer = createReducer(
    initialState,
    on(setUsers, (state, {data}) => {
       return ( {...state, users: data} )
    } ),
    on(setUser, (state, payload) => ({...state, user: payload}))
)

// export const userSelector = (state:UserState) => state

// Feature selector
export const userSelector = createFeatureSelector<UserState>('user')

export const selectorUsers = createSelector(
    userSelector,
    // (state:any) => state.user.users,

    // use feature selector
    (state:UserState) => state.users
)

export const selectorUser = createSelector(
    userSelector,
    // (state:any) => state.user

    // use feature selector
    (state: UserState) => state.user
)
