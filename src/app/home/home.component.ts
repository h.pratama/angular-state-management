import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import { UsersService } from '../services/users.service';
import { setUser, setUsers, User } from '../store/users/users.actions';
import { selectorUser, selectorUsers } from '../store/users/users.reducer';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  users:Observable<User[]>;
  user: Observable<User>;

  value: any;

  constructor(
    private userService: UsersService,
    private store: Store<any>
  ) { 
    this.users = this.store.select(selectorUsers)
    this.user = this.store.select(selectorUser)
  }

  ngOnInit(): void {
    this.userService.getUsers()

    // this.store.select('user').subscribe(stateUser => {
    //   console.log(stateUser, '<< state user');
      
    // })

    // this.user.subscribe(data => {
    //   console.log(data, '<< user');
    // })

    this.users.subscribe((data:any) => {
      this.value = data
    })
  }

  getUser(user: User) {
    this.store.dispatch(setUser(user))
  }

}
